var express = require('express');
var router = express.Router();
const Feedback = require('../models/Feedback');
const Register = require('../models/Login');
const Reguser = require('../models/Reguser');
const WholeReg = require('../models/Register');
const keys = require('../../config/keys');
var _ = require('lodash');
(cookieParser = require('cookie-parser')),
	(cookieSession = require('cookie-session'));

router.use(
	cookieSession({
		name: 'session',
		keys: [keys.cookieKey],
		maxAge: 24 * 60 * 60 * 1000,
	})
);
router.use(cookieParser());

router.route('/backend/postdata').get(function(req, res, next) {
	if (!req.session.token) {
		res.json({ login: 'Access Denied' });
	} else {
		Feedback.find(function(err, serverports) {
			res.json(serverports);
		});
	}
});

router.route('/backend/delete/:id').get(async (req, res, next) => {
	const deleteteam = await WholeReg.deleteOne({ reg_id: req.params.id })
		.then(data =>{
			Reguser.deleteMany({ reg_id: req.params.id }).then(data1 =>
					console.log(data1)
			)
		})
});

router.route('/backend/regcount').get(function(req, res, next) {
	WholeReg.find({ eventname: 'Rock n roll' }).then(users =>
		WholeReg.find({ eventname: 'இரட்டைக் ௧திரே' }).then(users1 =>
			WholeReg.find({ eventname: 'சித்திரம் பேசுதடி' }).then(users4 =>
				WholeReg.find({eventname: 'கண்ணாமூச்சி ரே.. ரே..' }).then(users3 =>
					WholeReg.find({ eventname: 'நெருப்பின்றி புகையாது' }).then(users11 =>
						WholeReg.find({ eventname: 'சிரிச்சா போச்சு [Comedy]' }).then(users12 =>
							WholeReg.find({ eventname: 'சிரிச்சா போச்சு [Mime]' }).then(
								users42 =>
									WholeReg.find({ eventname: 'நண்பேன்டா' }).then(
										users31 =>
											WholeReg.find({ eventname: 'ச..ரி..க..ம..ப.. [Solo Singing]' }).then(users111 =>
												WholeReg.find({ eventname: 'ச..ரி..க..ம..ப.. [Group Singing]' }).then(users112 =>
													WholeReg.find({ eventname: 'கலைமாமணி' }).then(users412 =>
														WholeReg.find({ eventname: 'Village விஞ்ஞானி' }).then(users312 =>
															WholeReg.find({ eventname: 'ஆளப்போறான் தமிழன்' }).then(users114 =>
																WholeReg.find({ eventname: 'MR & MS ORLIA' }).then(users128 =>
																	WholeReg.find({ eventname: 'ஒரு கதை சொல்லட்டா Sir?' }).then(
																		users429 =>
											res.status(200).json({
												Rock: users.length,
												twin: users1.length,
												Mime: users42.length,
												Comedy: users12.length,
												dump: users3.length,
												fireless: users11.length,
												friends: users31.length,
												arts: users312.length,
												pencil: users4.length,
												solo: users111.length,
												group: users112.length,
												orlia: users128.length,
												theatrix: users429.length,
												news: users412.length,
												exrempore: users114.length,
											})
									)
							)
						)
					)
				)
			)
		)
											)
									)
							)
						)
					)

			)
		)
	)
});

// router.route('/backend/reg').get(function(req, res, next) {
// 	if (!req.session.token) {
// 		res.json({ login: 'Access Denied' });
// 	} else {
// 		Reguser.find(function(err, serverports) {
// 			res.json(serverports);
// 		});
// 	}
// });

router.route('/backend/reg').get(async (req, res, next) => {
	const regusers = await Reguser.find({}).sort({_id: -1});
	if (regusers.length) {
		const results = [];
		const allUser = _.map(regusers, async reguser => {
			const wholeRegs = await WholeReg.findOne({ reg_id: reguser.reg_id });
			const result = _.merge(reguser, wholeRegs);
			results.push(result);
		});
		Promise.all(allUser).then(data => {
			res.send(results);
		});
	} else {
		res.send('No Events Found');
	}
});

router.route('/backend/year1').get(async (req, res, next) => {
	const regusers = await Reguser.find({'rollno': {'$regex': '18b'}}).sort({_id: -1});
	res.send(regusers);
});

router.route('/backend/year2').get(async (req, res, next) => {
	const regusers = await Reguser.find({'rollno': {'$regex': '17b'}}).sort({_id: -1});
	res.send(regusers);
});
router.route('/backend/year3').get(async (req, res, next) => {
	const regusers = await Reguser.find({'rollno': {'$regex': '16b'}}).sort({_id: -1});
	res.send(regusers);
});
router.route('/backend/year4').get(async (req, res, next) => {
	const regusers = await Reguser.find({'rollno': {'$regex': '15b'}}).sort({_id: -1});
	res.send(regusers);
});

router.route('/backend/eventwise').post(async (req, res, next) => {
	const { event_name } = req.body;
	const regusers = await Reguser.find({ eventname: event_name }).sort({_id: -1});
	if (regusers.length) {
		const results = [];
		const allUser = _.map(regusers, async reguser => {
			const wholeRegs = await WholeReg.findOne({ reg_id: reguser.reg_id });
			const result = _.merge(reguser, wholeRegs);
			results.push(result);
		});
		Promise.all(allUser).then(data => {
			res.send(results);
		});
	} else {
		res.send('No Events Found');
	}
});

router.route('/backend/deptwise').post(async (req, res, next) => {
	const { event_name, dept } = req.body;
	const regusers = await Reguser.find({ eventname: event_name, dept: dept });
	if (regusers.length) {
		const results = [];
		const allUser = _.map(regusers, async reguser => {
			const wholeRegs = await WholeReg.findOne({ reg_id: reguser.reg_id });
			const result = _.merge(reguser, wholeRegs);
			results.push(result);
		});
		Promise.all(allUser).then(data => {
			res.send(results);
		});
	} else {
		res.send('No Events Found');
	}
});

router.get('/backend/logout', (req, res) => {
	req.session = null;
	res.redirect('/');
});

router.get('/backend/user', (req, res, next) => {
	if (req.session.token) {
		res.json({
			status: 'session cookie set',
		});
	} else {
		res.json({
			status: 'Access Denied',
		});
	}
});

router.route('/backend/login').post(function(req, res, next) {
	const { name, password } = req.body;
	Register.find({ name: name, password: password }, (err, docs) => {
		if (docs.length) {
			if (docs[0].index == 1) {
				req.session.token = docs[0]._id;
				res.send(docs[0]);
			} else {
				res.send('Account is not verified');
			}
		} else {
			res.send('Account Not Found');
		}
	});
});

router.route('/backend/register').post(function(req, res, next) {
	const { name, dept, password, events } = req.body;
	const data = new Register({
		name,
		dept,
		events,
		password,
	});
	Register.find({ name: name }, (err, docs) => {
		if (!docs.length) {
			try {
				data
					.save()
					.then(data => {
						res.send('saved');
					})
					.catch(err => {
						res.send('Registration Failed');
					});
			} catch (err) {
				console.log('something wrong');
			}
		} else {
			res.send('User Already Exist');
		}
	});
});
module.exports = router;
