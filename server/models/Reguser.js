const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var Reguser = new Schema({
    reg_id:String,
    name: String,
    eventname: String,
    dept: String,
    rollno:String,
});

module.exports = mongoose.model('registeruser', Reguser);