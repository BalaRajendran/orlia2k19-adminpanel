var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Login = new Schema({
	name: String,
	dept: String,
	password: String,
	events: String,
	index: { type: Number, default: 0 },
	staff: { type: Number, default: 0 },
});

module.exports = mongoose.model('login', Login);
