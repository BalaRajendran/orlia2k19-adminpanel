// $(function () {
// 'use strict'
/* Morris.js Charts */
// Sales chart
// var d = new Date();
// var month = d.getMonth() + 1;
// var output = d.getFullYear() + '-' + (('' + month).length < 2 ? '0' : '') + month + '-' + "01";

var area = new Morris.Area({
  element: 'revenue-chart',
  resize: true,
  draggable: false,
  data: [{
      day: '2014-02-01',
      Purchases: 1,
    },
    // {
    //   day: '2014-02-02',
    //   Purchases: 2,
    // },
    // {
    //   day: '2014-02-03',
    //   Purchases: 3,
    // },
    // {
    //   day: '2014-02-04',
    //   Purchases: 4,
    // },
    // {
    //   day: '2014-02-05',
    //   Purchases: 5,
    // },
    // {
    //   day: '2014-02-06',
    //   Purchases: 6,
    // },
    // {
    //   day: '2014-02-07',
    //   Purchases: 7,
    // },
    // {
    //   day: '2014-02-08',
    //   Purchases: 8,
    // },
    // {
    //   day: '2014-02-09',
    //   Purchases: 9,
    // },
    // {
    //   day: '2014-02-10',
    //   Purchases: 10,
    // },
    // {
    //   day: '2014-02-11',
    //   Purchases: 11,
    // },
    // {
    //   day: '2014-02-12',
    //   Purchases: 12,
    // },
    // {
    //   day: '2014-02-13',
    //   Purchases: 13,
    // },
    // {
    //   day: '2014-02-14',
    //   Purchases: 14,
    // },
    // {
    //   day: '2014-02-15',
    //   Purchases: 15,
    // },
    // {
    //   day: '2014-02-16',
    //   Purchases: 16,
    // },
    // {
    //   day: '2014-02-17',
    //   Purchases: 17,
    // },
    // {
    //   day: '2014-02-18',
    //   Purchases: 18,
    // },
    // {
    //   day: '2014-02-19',
    //   Purchases: 19,
    // },
    // {
    //   day: '2014-02-20',
    //   Purchases: 20,
    // },
    // {
    //   day: '2014-02-21',
    //   Purchases: 22,
    // },
    // {
    //   day: '2014-02-22',
    //   Purchases: 23,
    // },
    // {
    //   day: '2014-02-23',
    //   Purchases: 24,
    // },
    // {
    //   day: '2014-02-24',
    //   Purchases: 25,
    // },
    // {
    //   day: '2014-02-25',
    //   Purchases: 26,
    // },
    // {
    //   day: '2014-02-26',
    //   Purchases: 27,
    // },
    // {
    //   day: '2014-02-27',
    //   Purchases: 28,
    // },
    // {
    //   day: '2014-02-28',
    //   Purchases: 29,
    // },
    // {
    //   day: '2014-02-29',
    //   Purchases: 30,
    // },
    // {
    //   day: '2014-02-30',
    //   Purchases: 31,
    // },
  ],
  xkey: 'day',
  ykeys: ['Purchases'],
  labels: ['Purchases'],
  lineColors: ['#007cff'],
  hideHover: 'auto'
})
var line = new Morris.Line({
  element: 'line-chart',
  resize: true,
  data: [{
      y: '2011 Q1',
      item1: 1
    },
    {
      y: '2011 Q2',
      item1: 2
    },
    {
      y: '2011 Q3',
      item1: 3
    },
    {
      y: '2011 Q4',
      item1: 4
    },
    {
      y: '2012 Q1',
      item1: 5
    },
    {
      y: '2012 Q2',
      item1: 6
    },
    {
      y: '2012 Q3',
      item1: 7
    },
    {
      y: '2012 Q4',
      item1: 8
    },
    {
      y: '2013 Q1',
      item1: 9
    },
    {
      y: '2013 Q2',
      item1: 2
    }
  ],
  xkey: 'y',
  ykeys: ['item1'],
  labels: ['Item 1'],
  lineColors: ['#efefef'],
  lineWidth: 2,
  hideHover: 'auto',
  gridTextColor: '#fff',
  gridStrokeWidth: 0.4,
  pointSize: 4,
  pointStrokeColors: ['#efefef'],
  gridLineColor: '#efefef',
  gridTextFamily: 'Open Sans',
  gridTextSize: 10
})

// Fix for charts under tabs
$('.box ul.nav a').on('shown.bs.tab', function () {
  area.redraw()
  line.redraw()
})
// })