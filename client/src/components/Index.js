import React, { Component } from 'react';
import TopSideNav from './layouts/Header/TopSideNav'
import axios from 'axios';

class Index extends Component {
	constructor(props) {
		super(props);
		this.state = {
			Comedy: '',
			Mime:"",
			Rock:"",
			arts:"",
			dump:"",
			exrempore:"",
			fireless:"",
			friends:"",
			group:"",
			news:"",
			orlia:"",
			pencil:"",
			solo:"",
			theatrix:"",
			twin:""
		};
	}
	componentWillMount(){
		axios
			.get('backend/user')
			.then(function (res) {
				if (res.data.status != "session cookie set") {
					window.location.replace('/');
				}
})
var self = this
		axios
			.get('/backend/regcount')
			.then(function (res) {
				console.log(res.data);
				console.log(res.data);
				self.setState({
					Comedy: res.data.Comedy,
					Mime: res.data.Mime,
					Rock: res.data.Rock,
					arts: res.data.arts,
					dump: res.data.dump,
					exrempore: res.data.exrempore,
					fireless: res.data.fireless,
					friends:res.data.friends,
					group: res.data.group,
					news: res.data.news,
					orlia:res.data.orlia,
					pencil:res.data.pencil,
					solo: res.data.solo,
					theatrix: res.data.theatrix,
					twin: res.data.twin
				});
	})
}
	render() {
		return (
			<div>
			<TopSideNav />
			<section className="content">
			{this.state.Rock && (
				<div className="container-fluid">
				
					<div className="row">
						<div className="col-lg-3 col-6">
							<div className="small-box bg-info">
								<div className="inner">
									<h3>{this.state.Rock}</h3>
									<p>Rock n roll</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-success">
								<div className="inner">
									<h3>{this.state.twin}</h3>
									<p>இரட்டைக் ௧திரே</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-warning">
								<div className="inner">
									<h3>{this.state.pencil}</h3>
									<p>சித்திரம் பேசுதடி</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-danger">
								<div className="inner">
									<h3>{this.state.dump}</h3>
									<p>கண்ணாமூச்சி ரே.. ரே..</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-lg-3 col-6">
							<div className="small-box bg-info">
								<div className="inner">
									<h3>{this.state.fireless}</h3>
									<p>நெருப்பின்றி புகையாது</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-success">
								<div className="inner">
									<h3>{this.state.Comedy}</h3>
									<p>சிரிச்சா போச்சு [Comedy]</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-warning">
								<div className="inner">
									<h3>{this.state.Mime}</h3>
									<p>சிரிச்சா போச்சு [Mime]</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-danger">
								<div className="inner">
									<h3>{this.state.friends}</h3>
									<p>நண்பேன்டா</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-lg-3 col-6">
							<div className="small-box bg-info">
								<div className="inner">
									<h3>{this.state.solo}</h3>
									<p>ச..ரி..க..ம..ப.. [Solo Singing]</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-success">
								<div className="inner">
									<h3>{this.state.group}</h3>
									<p>ச..ரி..க..ம..ப.. [Group Singing]</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-warning">
								<div className="inner">
									<h3>{this.state.news}</h3>
									<p>கலைமாமணி</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-danger">
								<div className="inner">
									<h3>{this.state.arts}</h3>
									<p>Village விஞ்ஞானி</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-lg-3 col-6">
							<div className="small-box bg-info">
								<div className="inner">
									<h3>{this.state.exrempore}</h3>
									<p>ஆளப்போறான் தமிழன் / Basically I'm from california</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-success">
								<div className="inner">
									<h3>{this.state.orlia}</h3>
									<p>MR & MS ORLIA</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
						<div className="col-lg-3 col-6">
							<div className="small-box bg-warning">
								<div className="inner">
									<h3>{this.state.theatrix}</h3>
									<p>ஒரு கதை சொல்லட்டா Sir?</p>
								</div>
								<div className="icon">
									<i className="ion ion-stats-bars"></i>
								</div>
							</div>
						</div>
					</div>
				
				</div>
				)}
				</section>
			</div>
		);
	}
}
export default Index;
