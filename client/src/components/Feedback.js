import React, { Component } from 'react';
import axios from 'axios';
import TableRow from './layouts/View/TableRow';

class Feedback extends Component {
	constructor(props) {
		super(props);
		this.state = { serverports1: [], login: '' };
	}
	componentWillMount(){
				axios
					.get('backend/postdata')
					.then(response => {
						if (
							response.data.login === 'Access Denied'
						) {
							alert("Access Denied");
							window.location.replace('/');
							this.setState({ login: response.data.login });
						} else if (response.data.login == "No Events Found") {
							this.setState({ serverports1: response.data });
						}  else {
							this.setState({ serverports1: response.data });
						}
					})
					.catch(function (error) {
						console.log(error);
					});
	}

    tabRow() {
        return this.state.serverports1.map(function (object, i) {
            return <TableRow obj={object} i={i} key={i} />;
        });
    }
	render() {
		return (
			<div>
				<section className="content-header">
					<div className="container-fluid">
						<div className="row mb-2">
							<div className="col-sm-6">
								<h1>Feedback</h1>
							</div>
						</div>
					</div>
				</section>
				<section className="content">
					<div className="row">
						<div className="col-12">
							<div className="card">
								<div className="card-body">
									<div className="table-responsive">
										<table
											id="example2"
											className="table table-bordered table-striped"
										>
											<thead>
												<tr>
													<th>S.No</th>
                                                    <th>Name</th>
                                                    <th>Roll No</th>
                                                    <th>Phone</th>
													<th>Message</th>
												</tr>
											</thead>
											{this.state.serverports1 != "No Events Found" && (
												<tbody>{this.tabRow()}</tbody>
											)}
											{this.state.serverports1 == "No Events Found" && (
												<td><tr style={{ textAlign: "center" }}>No Events Found</tr></td>
											)}
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		);
	}
}
export default Feedback;
