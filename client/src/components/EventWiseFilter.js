import React, { Component } from 'react';
import axios from 'axios';
import TableRow from './layouts/View/TableRowEvent';

class Feedback extends Component {
	constructor(props) {
		super(props);
		this.state = { event_name:"",serverports1: [], login: '' };
	}
	componentWillMount(){
		let url = window.location.href;
		let split = url.split("?")
		if (split[1] == "Rock") {
			url = "Rock n roll"
		} else if (split[1] == "TWIN") {
			url = "இரட்டைக் ௧திரே"
		} else if (split[1] == "PENCIL") {
			url = "சித்திரம் பேசுதடி"
		} else if (split[1] == "DUMB") {
			url = "கண்ணாமூச்சி ரே.. ரே.."
		} else if (split[1] == "FIRELESS") {
			url = "நெருப்பின்றி புகையாது"
		} else if (split[1] == "Comedy") {
			url = "சிரிச்சா போச்சு [Comedy]"
		} else if (split[1] == "Mime") {
			url = "சிரிச்சா போச்சு [Mime]"
		} else if (split[1] == "friends") {
			url = "நண்பேன்டா"
		} else if (split[1] == "Solo") {
			url = "ச..ரி..க..ம..ப.. [Solo Singing]"
		} else if (split[1] == "Group") {
			url = "ச..ரி..க..ம..ப.. [Group Singing]"
		} else if (split[1] == "Newspaper") {
			url = "கலைமாமணி"
		} else if (split[1] == "Basically") {
			url = "ஆளப்போறான் தமிழன்"
		} else if (split[1] == "Arts") {
			url = "Village விஞ்ஞானி"
		} else if (split[1] == "Short") {
			url = "ஒரு கதை சொல்லட்டா Sir?"
		} else if (split[1] == "Orlia") {
			url = "MR & MS ORLIA"
		}
		const data = {
			event_name : url
		}
				axios
					.post('backend/eventwise',data)
					.then(response => {
						if (
							response.data.login === 'Access Denied'
						) {
							alert("Access Denied");
							window.location.replace('/');
							this.setState({ login: response.data.login });
						} else if (response.data.login == "No Events Found") {
							this.setState({ event_name: url, serverports1: response.data });
						} else {
							console.log(response.data)
							this.setState({event_name:url, serverports1: response.data });
						}
					})
					.catch(function (error) {
						console.log(error);
					});
	}

    tabRow() {
        return this.state.serverports1.map(function (object, i) {
            return <TableRow obj={object} i={i} key={i} />;
        });
    }
	render() {
		return (
			<div>
				<section className="content-header">
					<div className="container-fluid">
						<div className="row mb-2">
							<div className="col-sm-6">
								<h1>Event Wise Split Up</h1>
								<h3>Event Name : { this.state.event_name}</h3>
							</div>
						</div>
					</div>
				</section>
				<section className="content">
					<div className="row">
						<div className="col-12">
							<div className="card">
								<div className="card-body">
									<div className="table-responsive">
										<table
											id="example2"
											className="table table-bordered table-striped"
										>
											<thead>
												<tr>
													<th>S.No</th>
													<th>Team Name</th>
													<th>Name</th>
													<th>Register Number</th>
													<th>Event Name</th>
													<th>Department</th>
													<th>Action</th>
												</tr>
											</thead>
											{this.state.serverports1 != "No Events Found" && (
												<tbody>{this.tabRow()}</tbody>
											)}
											{this.state.serverports1 == "No Events Found" && (
												<tbody>
													<tr><td colSpan="7" style={{ textAlign: "center" }}>No Events Found</td></tr>
												</tbody>
											)}
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		);
	}
}
export default Feedback;
