import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import TopNav from './layouts/Header/TopNav';
import Nav from './layouts/Header/Index';
import Footer from './layouts/Footer/Footer';
import Reg from './Reg';
import Index from './Index';
import Feedback from './Feedback';
import Login from './Login'
import Register from './Register';
import Eventwise from './EventWise'
import EventwiseFilter from './EventWiseFilter';
import Deptwise from './DeptWise';
import DeptwiseFilter from './DeptWiseFilter';

class App extends Component {
	render() {
		return (
			<div className="wrapper">
				<TopNav />
				<Nav />
				<div className="content-wrapper">
					<Switch>
						<Route path="/dashboard" component={Index} exact />
						<Route path="/" component={Login} exact />
						<Route path="/register" component={Register} exact />
						<Route path="/reg" component={Reg} exact />
						<Route path="/feedback" component={Feedback} exact />
						<Route path="/eventwise" component={Eventwise} exact />
						<Route path="/Deptwise" component={Deptwise} exact />
						<Route path="/eventwisefilter" component={EventwiseFilter} exact />
						<Route path="/deptwisefilter" component={DeptwiseFilter} exact />
					</Switch>
				</div>
				<Footer />
			</div>
		);
	}
}
export default App;
