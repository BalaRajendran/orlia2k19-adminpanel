import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
var $ = require('jquery');

class Register extends Component {
	constructor(props) {
		super(props);
		this.onChangeName = this.onChangeName.bind(this);
		this.onChangeDept = this.onChangeDept.bind(this);
		this.onChangePassword = this.onChangePassword.bind(this);
		this.onChangeEvents = this.onChangeEvents.bind(this);
		this.onChangeRePassword = this.onChangeRePassword.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state = {
			name: '',
			dept: '',
			password: '',
			repassword: '',
			events: "",
			error:""
		};
	}
	componentWillMount() {
		$("body").removeClass("sidebar-open")
		$("body").addClass("sidebar-collapse")
		axios
			.get('backend/user')
			.then(function (res) {
				if (res.data.status == "session cookie set") {
					window.location.replace('/dashboard');
				}
				else {
					$(".checksetlogin").addClass("hide")
					cookies.remove("name", { path: '/' })
					cookies.remove("dept", { path: '/' })
					cookies.remove("staff", { path: '/' })

				}
			})
	}
	onChangeEvents(e) {
		this.setState({
			events: e.target.value,
			error: ""
		});
	}
	onChangeName(e) {
		this.setState({
			name: e.target.value,
			error:""
		});
	}
	onChangeDept(e) {
		this.setState({
			dept: e.target.value,
			error: ""
		});
	}
	onChangePassword(e) {
		this.setState({
			password: e.target.value,
			error: ""
		});
		if (this.state.password != e.target.value) {
			this.setState({
				error: "Password Not Match",
			});
		}
	}
	onChangeRePassword(e) {
		this.setState({
			repassword: e.target.value,
			error: ""
		});
		if (this.state.password != e.target.value) {
			this.setState({
				error: "Password Not Match",
			});
		}
	}
	onSubmit(e) {
		e.preventDefault();
		let k = 0;
			if (!this.state.name) {
				this.setState({
					error: 'Name Required',
				});console.log("fff")
				k = 1
		}
		if (k == 0) {
			if (!this.state.dept) {
				this.setState({
					error: 'Dept Required',
				});
				k = 1
			}
		}
		if (k == 0) {
			if (!this.state.events) {
				this.setState({
					error: 'Events Required',
				});
				k = 1
			}
		}
		if (k == 0) {
		if (!this.state.password) {
			this.setState({
				error: 'Password Required',
			});
			k = 1
		}
	}
		if (k == 0) {
			if (!this.state.repassword) {
				this.setState({
					error: 'Retype Password Required',
				});
				k = 1
			}
		}
		if (k == 0) {
			if (this.state.repassword != this.state.password) {
				this.setState({
					error: 'Password Not Match',
				});
				k = 1
			}
		}
		if (k == 0) {
			this.setState({
				error: '',
			});
			const data = {
				name: this.state.name,
				dept: this.state.dept,
				password: this.state.password,
				events:this.state.events,
				repassword: this.state.repassword
			};
			var self = this
			axios
				.post('backend/register', data)
				.then(function (res) {
					if (res.data == "saved") {
						alert("Registered Sucessfully");
						self.setState({
							name: '',
							password: '',
							repassword: '',
							dept: '',
							error: '',
							events:""
						});
					} else {
						alert(res.data);
					}
				})
				.catch(function (error) { });
		}
	}
	render() {
        return (
			<div className="register-box">
				<div className="register-logo">
					<a href="">
						<b>Register</b>
					</a>
				</div>

				<div className="card">
					<div className="card-body register-card-body">
						<p className="login-box-msg">Register a new membership</p>
						<form onSubmit={this.onSubmit}>
							{this.state.error && (
								<div className="alert alert-danger">
									<strong>{this.state.error}</strong>
								</div>
							)}
							<div className="form-group has-feedback">
								<input
									type="text"
									name="username"
									value={this.state.name}
									onChange={this.onChangeName}
									className="form-control"
									placeholder="User name"
								/>
							</div>
							<div className="form-group has-feedback">
								<select
									value={this.state.dept}
									onChange={this.onChangeDept} 
									className="form-control"
								>
									<option value="">Choose Dept</option>
									<option value="it">B.TECH-IT</option>
									<option value="cs">BE-CSE</option>
									<option value="ee">BE-EEE</option>
									<option value="ce">BE-CIVIL</option>
									<option value="ec">BE-ECE</option>
									<option value="ei">BE-EIE</option>
									<option value="me">BE-MECH</option>
									<option value="ca">MCA</option>
									<option value="OTHERS">OTHERS</option>
								</select>
							</div>
							<div className="form-group has-feedback">
								<select
									value={this.state.events}
									onChange={this.onChangeEvents} 
									className="form-control"
								>
									<option value="">Events Optional</option>
									<option value="Rock n roll">Rock n' roll (GROUP DANCE)</option>
									<option value="இரட்டைக் ௧திரே">இரட்டைக் ௧திரே (TWIN ATTACK)</option>
									<option value="சித்திரம் பேசுதடி">சித்திரம் பேசுதடி [PENCIL SKETCHING]</option>
									<option value="கண்ணாமூச்சி ரே.. ரே..">கண்ணாமூச்சி ரே.. ரே.. [DUMB CHARADES]</option>
									<option value="நெருப்பின்றி புகையாது">நெருப்பின்றி புகையாது [FIRELESS COOKING]</option>
									<option value="சிரிச்சா போச்சு [Comedy]">சிரிச்சா போச்சு[Comedy]</option>
									<option value="சிரிச்சா போச்சு [Mime]">சிரிச்சா போச்சு[Mime]</option>
									<option value="நண்பேன்டா">நண்பேன்டா [BEST BUDDIES]</option>
									<option value="ச..ரி..க..ம..ப.. [Solo Singing]">ச..ரி..க..ம..ப.. [Solo Singing]</option>
									<option value="ச..ரி..க..ம..ப.. [Group Singing]">ச..ரி..க..ம..ப.. [Group Singing]</option>
									<option value="கலைமாமணி">கலைமாமணி[Newspaper Dressing,Hair Dressing,Mehandhi,Nail Art,Face Painting]</option>
									<option value="ஆளப்போறான் தமிழன்">ஆளப்போறான் தமிழன்/Basically I'm from  California</option>
									<option value="Village விஞ்ஞானி">Village விஞ்ஞானி[Arts From Waste]</option>
									<option value="MR & MS ORLIA">MR & MS ORLIA</option>
									<option value="ஒரு கதை சொல்லட்டா Sir?">ஒரு கதை சொல்லட்டா Sir? [Short Film]</option>
									<option value="no">No Events</option>
								</select>
							</div>
							<div className="form-group has-feedback">
								<input
									type="password"
									name="password"
									className="form-control"
									value={this.state.password}
									onChange={this.onChangePassword}
									placeholder="Password"
								/>
							</div>
							<div className="form-group has-feedback">
								<input
									type="password"
									name="password"
									value={this.state.repassword}
									onChange={this.onChangeRePassword}
									className="form-control"
									placeholder="Retype password"
								/>
							</div>
							<div className="row">
								<div className="col-4">
									<button
										type="submit"
										className="btn btn-primary btn-block btn-flat"
									>
										Register
									</button>
								</div>
							</div>
						</form>

						<a href="/" className="text-center">
							I want to login
						</a>
					</div>
				</div>
                </div>
		);
	}
}
export default Register;
