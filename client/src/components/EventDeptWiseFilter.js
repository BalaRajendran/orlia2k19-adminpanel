import React, { Component } from 'react';
import axios from 'axios';
import TableRow from './layouts/View/TableRowEventDept';

class Feedback extends Component {
	constructor(props) {
		super(props);
		this.state = { event_name:"",serverports1: [], login: '' };
	}
	componentWillMount(){
		let url1 = window.location.href;
		let split = url1.split("?")
		let url;
		let url2 = split[1].split("&")
		if (url2[0] == "Rock") {
			url = "Rock n roll"
		} else if (url2[0] == "TWIN") {
			url = "இரட்டைக் ௧திரே"
		} else if (url2[0] == "PENCIL") {
			url = "சித்திரம் பேசுதடி"
		} else if (url2[0] == "DUMB") {
			url = "கண்ணாமூச்சி ரே.. ரே.."
		} else if (url2[0] == "FIRELESS") {
			url = "நெருப்பின்றி புகையாது"
		} else if (url2[0] == "Comedy") {
			url = "சிரிச்சா போச்சு [Comedy]"
		} else if (url2[0] == "Mime") {
			url = "சிரிச்சா போச்சு [Mime]"
		} else if (url2[0] == "friends") {
			url = "நண்பேன்டா"
		} else if (url2[0] == "Solo") {
			url = "ச..ரி..க..ம..ப.. [Solo Singing]"
		} else if (url2[0] == "Group") {
			url = "ச..ரி..க..ம..ப.. [Group Singing]"
		} else if (url2[0] == "Newspaper") {
			url = "கலைமாமணி"
		} else if (url2[0] == "Basically") {
			url = "ஆளப்போறான் தமிழன்"
		} else if (url2[0] == "Arts") {
			url = "Village விஞ்ஞானி"
		} else if (url2[0] == "Short") {
			url = "ஒரு கதை சொல்லட்டா Sir?"
		} else if (url2[0] == "Orlia") {
			url = "MR & MS ORLIA"
		}
		const data = {
			event_name: url,
			dept:url2[1]
		}
		console.log(data)
				axios
					.post('backend/deptwise',data)
					.then(response => {
						if (
							response.data.login === 'Access Denied'
						) {
							alert("Access Denied");
							window.location.replace('/');
							this.setState({ login: response.data.login });
						} else if (response.data.login == "No Events Found") {
							this.setState({ event_name: url, serverports1: response.data });
						} else {
							console.log(response.data)
							this.setState({event_name:url,dept:url2[1], serverports1: response.data });
						}
					})
					.catch(function (error) {
						console.log(error);
					});
	}

    tabRow() {
        return this.state.serverports1.map(function (object, i) {
            return <TableRow obj={object} i={i} key={i} />;
        });
    }
	render() {
		return (
			<div>
				<section className="content-header">
					<div className="container-fluid">
						<div className="row mb-2">
							<div className="col-sm-6">
								<h1>Event Wise Split Up</h1>
								<h3>Event Name : {this.state.event_name}</h3>
								<h3>Dept : { this.state.dept}</h3>
							</div>
						</div>
					</div>
				</section>
				<section className="content">
					<div className="row">
						<div className="col-12">
							<div className="card">
								<div className="card-body">
									<div className="table-responsive">
										<table
											id="example2"
											className="table table-bordered table-striped"
										>
											<thead>
												<tr>
													<th>S.No</th>
													<th>Team Name</th>
													<th>Name</th>
													<th>Register Number</th>
													<th>Event Name</th>
													<th>Department</th>
												</tr>
											</thead>
											{this.state.serverports1 != "No Events Found" && (
												<tbody>{this.tabRow()}</tbody>
											)}
											{this.state.serverports1 == "No Events Found" && (
												<tbody>
													<td><tr style={{ textAlign: "center" }}>No Events Found</tr></td>
												</tbody>
											)}
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		);
	}
}
export default Feedback;
