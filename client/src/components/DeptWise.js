import React, { Component } from 'react';
import axios from 'axios';

class EventWise extends Component {
	constructor(props) {
		super(props);
		this.onChangeEvent = this.onChangeEvent.bind(this);
		this.onChangeDept = this.onChangeDept.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state = {
			event_name: '',
			dept:"",
			serverports1:[]
		};
	}
	componentWillMount() {
		axios
			.get('backend/user')
			.then(function (res) {
				if (res.data.status != "session cookie set") {
					window.location.replace('/');
				}
			})
	}
	onChangeEvent(e) {
		this.setState({
			event_name: e.target.value,
		});
	}
	onChangeDept(e) {
		this.setState({
			dept: e.target.value,
		});
	}
	onSubmit(e) {
		e.preventDefault();
		if (!this.state.event_name) {
			alert('Choose Event Name');
		} if (!this.state.dept) {
			alert('Choose Dept');
		} else {
			window.location.replace('/deptwisefilter?'+this.state.event_name+'&'+this.state.dept);
		}
	}
	render() {
		return (
			<section className="content">
				<div className="container">
				<form onSubmit={this.onSubmit} className="lgx-contactform">
						<div className="row">
							<div className="col-md-4" style={{ marginTop: "160px" }}>
								<div className="form-group">
									<label>Choose Department*</label>
								<select
									name="cars"
									value={this.state.dept}
										className="form-control"
									onChange={this.onChangeDept}
								>
									<option className="my-form" value="">
										Department
												</option>
										<option value="">Choose Dept</option>
										<option value="it">B.TECH-IT</option>
										<option value="cs">BE-CSE</option>
										<option value="ee">BE-EEE</option>
										<option value="ce">BE-CIVIL</option>
										<option value="ec">BE-ECE</option>
										<option value="ei">BE-EIE</option>
										<option value="me">BE-MECH</option>
										<option value="ca">MCA</option>
										<option value="cm">ME</option>
										<option value="vl">VL</option>
								</select>
								</div>
							</div>
							<div className="col-md-4" style={{ marginTop: "160px" }}>
								<div className="form-group">
									<label>Choose Event*</label>
									<select
										className="form-control"
										onChange={this.onChangeEvent}
										value={this.state.event_name}
									>
										<option value="">Choose Event</option>
										<option value="Rock">
											Rock n' roll (GROUP DANCE)
										</option>
										<option value="TWIN">
											இரட்டைக் ௧திரே (TWIN ATTACK)
										</option>
										<option value="PENCIL">
											சித்திரம் பேசுதடி [PENCIL SKETCHING]
										</option>
										<option value="DUMB">
											கண்ணாமூச்சி ரே.. ரே.. [DUMB CHARADES]
										</option>
										<option value="FIRELESS">
											நெருப்பின்றி புகையாது [FIRELESS COOKING]
										</option>
										<option value="Comedy">
											சிரிச்சா போச்சு[Comedy]
										</option>
										<option value="Mime">
											சிரிச்சா போச்சு[Mime]
										</option>
										<option value="friends">நண்பேன்டா [BEST BUDDIES]</option>
										<option value="Solo">
											ச..ரி..க..ம..ப.. [Solo Singing]
										</option>
										<option value="Group">
											ச..ரி..க..ம..ப.. [Group Singing]
										</option>
										<option value="Newspaper">
											கலைமாமணி[Newspaper Dressing,Hair Dressing,Mehandhi,Nail
											Art,Face Painting]
										</option>
										<option value="Basically">
											ஆளப்போறான் தமிழன்/Basically I'm from California
										</option>
										<option value="Arts">
											Village விஞ்ஞானி[Arts From Waste]
										</option>
										<option value="Orlia">MR & MS ORLIA</option>
										<option value="Short">
											ஒரு கதை சொல்லட்டா Sir? [Short Film]
										</option>
									</select>
								</div>
							</div>
							<div className="col-md-3">
								<button
									className="btn btn-primary"
									style={{ marginTop: '190px' }}
									type="submit"
									name="search"
									id="search"
								>
									Search
								</button>
							</div>
						</div>
					</form>
					</div>
			</section>
		);
	}
}
export default EventWise;
