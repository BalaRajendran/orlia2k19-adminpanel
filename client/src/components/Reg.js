import React, { Component } from 'react';
import axios from 'axios';
// import Table from './layouts/View/TableRowReg';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, {
  Search,
  CSVExport
} from 'react-bootstrap-table2-toolkit';
const { SearchBar } = Search;
const { ExportCSVButton } = CSVExport;
const pagination = paginationFactory({
  page: 1,
  alwaysShowAllBtns: true,
  showTotal: true,
  withFirstAndLast: false,
  sizePerPageRenderer: ({ options, currSizePerPage, onSizePerPageChange }) => (
    <div className='dataTables_length' id='datatable-basic_length'>
      <label>
        Show{' '}
        {
          <select
            name='datatable-basic_length'
            aria-controls='datatable-basic'
            className='form-control form-control-sm'
            onChange={e => onSizePerPageChange(e.target.value)}
          >
            <option value='10'>10</option>
            <option value='25'>25</option>
            <option value='50'>50</option>
            <option value='100'>100</option>
          </select>
        }{' '}
        entries.
      </label>
    </div>
  )
});

// const MySearch = props => {
//   let input;
//   const handleClick = () => {
//     props.onSearch(input.value);
//   };
//   return (
//     <GridContainer
//       style={{
//         marginTop: '5px',
//         marginBottom: '10px',
//         justifyContent: 'space-between'
//       }}
//     >
//       <GridItem md={3} sm={12} xs={12}>
//         <input
//           autoFocus
//           autoComplete='off'
//           className='form-control input-userstable'
//           ref={n => (input = n)}
//           type='text'
//           placeholder='Search attributes'
//           onChange={handleClick}
//         />
//       </GridItem>
//     </GridContainer>
//   );
// };

class Reg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serverports1: [],
      login: '',
      dataTable: [],
      rows: [],
      columns: [],
      data: ''
    };
  }
  componentDidMount() {
    let tableRows = [];
    let mTableHead = [
      {
        dataField: 'name',
        text: 'Student Name',
        sort: true
      },
      {
        dataField: 'rollno',
        text: 'Roll No',
        sort: true
      },
      {
        dataField: 'eventname',
        text: 'Event Name',
        sort: true
      },
      {
        dataField: 'dept',
        text: 'Department',
        sort: true
      },
      {
        dataField: 'teamname',
        text: 'Teamname',
        sort: true
      },

      {
        dataField: 'dateSent',
        text: 'Registered on',
        sort: true
      },
      {
        dataField: 'count',
        text: 'Count',
        sort: true
      }
    ];
    let tableHead = [...mTableHead];
    axios
      .get('backend/reg')
      .then(response => {
        if (response.data.login === 'Access Denied') {
          alert('Access Denied');
          window.location.replace('/');
          this.setState({ login: response.data.login });
        } else {
          this.setState({ serverports1: response.data });
          if (mTableHead.length > 0) {
            response.data.map((choice, i) => {
              let row = {
                id: i,
                name: choice.name,
                rollno: choice.rollno,
                eventname: choice.eventname,
                teamname: choice.teamname,
                dept: choice.dept,
                dateSent: choice.dateSent,
                count: choice.count
              };
              return tableRows.push(row);
            });
          }
        }
        console.log(tableRows, tableHead);
        this.setState({
          rows: tableRows,
          columns: tableHead
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  ConvertToCSV = () => {
    let users = [
      'Student Name,Roll No,Event Name,Department,Teamname,Registered on,Count\r\n'
    ];
    this.state.rows.map((field, index) => {
      users += [
        `"${field.name}","${field.rollno}","${field.eventname}","${field.dept}","${field.teamname}","${field.dateSent}","${field.count}"\r\n`
      ];
    });
    const blob = new Blob(['\ufeff' + users], {
      type: 'text/csv;charset=utf-8;'
    });
    const dwldLink = document.createElement('a');
    const url = URL.createObjectURL(blob);
    const isSafariBrowser =
      navigator.userAgent.indexOf('Safari') !== -1 &&
      navigator.userAgent.indexOf('Chrome') === -1;

    if (isSafariBrowser) {
      dwldLink.setAttribute('target', '_blank');
    }
    dwldLink.setAttribute('href', url);
    dwldLink.setAttribute('download', `orlia2k19.csv`);
    dwldLink.style.visibility = 'hidden';
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
    // return users;
  };
  render() {
    return (
      <div>
        <section className='content-header'>
          <div className='container-fluid'>
            <div className='row mb-2'>
              <div className='col-sm-6'>
                <h1>Registration</h1>
              </div>
            </div>
          </div>
        </section>
        <section className='content'>
          <div className='row'>
            <div className='col-12'>
              <div className='card'>
                <div className='card-body'>
                  {this.state.rows.length > 1 ? (
                    <div>
                      <ToolkitProvider
                        data={this.state.rows}
                        keyField='id'
                        columns={this.state.columns}
                        search
                        exportCSV
                      >
                        {props => (
                          <div>
                            <div
                              id='datatable-basic_filter'
                              className='dataTables_filter pb-1'
                            >
                              <label>
                                <SearchBar
                                  className='form-control-sm'
                                  placeholder=''
                                  {...props.searchProps}
                                />
                              </label>
                              <button
                                className='btn btn-primary'
                                onClick={() => {
                                  this.ConvertToCSV();
                                }}
                              >
                                Export CSV!!
                              </button>
                              {/* <ExportCSVButton {...props.csvProps}>
                              Export CSV!!
                            </ExportCSVButton> */}
                            </div>
                            <BootstrapTable
                              noDataIndication={<center>No data found</center>}
                              {...props.baseProps}
                              bootstrap4={true}
                              pagination={pagination}
                              bordered={false}
                            />
                          </div>
                        )}
                      </ToolkitProvider>
                    </div>
                  ) : (
                    'Loading....'
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default Reg;
