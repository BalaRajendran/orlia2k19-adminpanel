import React, { Component } from 'react';

class TableRow extends Component {
  render() {  
    return (
          <tr>
          <td>
            {this.props.i+1}
          </td>
          <td>
            {this.props.obj.name}
        </td>
        <td>
          {this.props.obj.rollno}
        </td>
        <td>
          {this.props.obj.phone}
        </td>
        <td>
          {this.props.obj.message}
        </td>
          </tr>
    );
  }
}

export default TableRow;