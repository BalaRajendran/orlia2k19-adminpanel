import React, { Component } from 'react';
import axios from 'axios';

class TableRow extends Component {
     constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
    }
    delete() {
        axios.get('backend/delete/' + this.props.obj.reg_id)
            .then(window.location.reload())
            .catch(err => console.log(err))
    }
    render() {
        return (
            <tr>
                <td>
                    {this.props.i + 1}
                </td>
                <td>
                    {this.props.obj.teamname}
                </td>
                <td>
                    {this.props.obj.name}
                </td>
                <td>
                    {this.props.obj.rollno}
                </td>
                <td>
                    {this.props.obj.dept}
                </td>
                <td>
                    {this.props.obj.dateSent}
                </td>
                <td>
                    <button  onClick={this.delete}  className="btn btn-danger"><i className='fa fa-trash-o'></i></button>
                </td>
            </tr>
        );
    }
}

export default TableRow;