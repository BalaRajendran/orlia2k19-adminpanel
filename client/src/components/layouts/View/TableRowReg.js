import React, { Component } from 'react';

class TableRow extends Component {
    render() {
        return (
            <tr>
                <td>
                    {this.props.i + 1}
                </td>
                <td>
                    {this.props.obj.name}
                </td>
                 <td>
                    {this.props.obj.teamname}
                </td>
                <td>
                    {this.props.obj.rollno}
                </td>
                <td>
                    {this.props.obj.dept}
                </td>
                <td>
                    {this.props.obj.eventname}
                </td>
                <td>
                    {this.props.obj.dateSent}
                </td>
            </tr>
        );
    }
}

export default TableRow;