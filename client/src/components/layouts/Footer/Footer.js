import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="main-footer">
                <strong>Copyright &copy; 2018-2019 <a target="_blank" href="https://orlia2k19.herokuapp.com">Orlia-2k19</a></strong>
            </footer>
        )
    }
}
export default Footer;