import React, { Component } from 'react';

class TopNav extends Component {
    render() {
        return (
            <nav className="main-header navbar navbar-expand bg-white navbar-light border-bottom">
                <ul className="navbar-nav">
                    <li id="nav-item"  className="nav-item">
                        <a  className="nav-link hide" data-widget="pushmenu" href="#"><i className="fa fa-bars"></i></a>
                    </li>
                    <li className="nav-item d-none d-sm-inline-block">
                        <a href="https://orlia2k19.herokuapp.com" target="_blank" className="nav-link">Home Website</a>
                    </li>
                </ul>
                <ul className="navbar-nav checksetlogin ml-auto">
                <li className="nav-item dropdown">
                    <a className="nav-link  back" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Log Out
                        </a>
                    <div className="dropdown-menu width dropdown-menu-right">
                        <ul className="dropdown-user user-header">
                                <p style={{ marginLeft: "20px" }}>
                                Admin
                            </p>
                            <a href="/backend/logout" className="btnpic btn-default">Sign out</a> 
                        </ul>
                    </div>
                </li>
            </ul >
            </nav>
        )
    }
}
export default TopNav;