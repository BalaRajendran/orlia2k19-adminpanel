import React, { Component } from 'react';

class Nav extends Component {
    render() {
        return (
            <nav className="mt-2">
                <ul
                    className="nav nav-pills nav-sidebar flex-column"
                    data-widget="treeview"
                    role="menu"
                    data-accordion="false"
                >
                    <li className="nav-item">
                        <a href="/dashboard" className="nav-link">
                            <i className="nav-icon fa fa-th" />
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="/reg" className="nav-link ">
                            <i className="nav-icon fa fa-dashboard" />
                            <p>
                              OverAll  Registration
                            </p>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="/eventwise" className="nav-link">
                            <i className="nav-icon fa fa-th" />
                            <p>Event Wise List</p>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="/deptwise" className="nav-link">
                            <i className="nav-icon fa fa-th" />
                            <p>Event & Dept Wise</p>
                        </a>
                    </li>
                    
                        <li className="nav-item">
                            <a href="/feedback" className="nav-link">
                            <i className="nav-icon fa fa-dashboard" />
                                <p>Feedback</p>
                            </a>
                        </li>
                </ul>
            </nav>
        );
    }
}
export default Nav;