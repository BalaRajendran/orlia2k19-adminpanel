import React, { Component } from 'react';
import Navigation from './Nav';

class Nav extends Component {
	render() {
		return (
			<aside className="main-sidebar sidebar-dark-primary elevation-4">
				<div className="sidebar">
					<div className="user-panel mt-3 pb-3 mb-3 d-flex">
						<div className="image">
							<img
								src="dist/img/AdminLTELogo.png"
								className="img-circle elevation-2"
								alt="User Image"
							/>
						</div>
						<div className="info">
							<a href="#" className="d-block">
								Admin
							</a>
						</div>
					</div>
					<Navigation />
				</div>
			</aside>
		);
	}
}
export default Nav;
