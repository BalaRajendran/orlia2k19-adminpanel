import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
var $ = require('jquery');

class Login extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            name: '',
            password: '',
            error: "",
            staff: "",
            dept:""
        };
    }
    componentWillMount() {
        $("#nav-item").addClass("hide");
        $("body").removeClass("sidebar-open")
        $("body").addClass("sidebar-collapse")
        axios
            .get('backend/user')
            .then(function (res) {
                if (res.data.status == "session cookie set") {
                    window.location.replace('/dashboard');
                }
                else {
                    $(".checksetlogin").addClass("hide")
                    cookies.remove("name", {path:'/'})
                    cookies.remove("dept", { path: '/' })
                    cookies.remove("staff", {path:'/'})

                }
            })
    }
    onChangeName(e) {
        this.setState({
            name: e.target.value,
            error: ""
        });
    }
    onChangePassword(e) {
        this.setState({
            password: e.target.value,
            error: ""
        });
    }
    onSubmit(e) {
        e.preventDefault();
        let k = 0;
        if (!this.state.name) {
            this.setState({
                error: 'Name Required',
            });
            k = 1
        }
        if (k == 0) {
            if (!this.state.password) {
                this.setState({
                    error: 'Password Required',
                });
                k = 1
            }
        }
        if (k == 0) {
            this.setState({
                error: '',
            });
            const data = {
                name: this.state.name,
                password: this.state.password,
            };
            var self = this;
            axios
                .post('backend/login', data)
                .then(function (res) {
                    if (res.data != "Account is not verified" && res.data != "Account Not Found") {
                        self.setState({
                            name: res.data.name,
                            dept: res.data.dept,
                            staff: res.data.staff
                        });
                        cookies.set('dept', res.data.dept, { path: '/' });
                        cookies.set('name', res.data.name, { path: '/' });
                        cookies.set('staff', res.data.staff, { path: '/' });
                        window.location.replace('/');
                    } else {
                        alert(res.data)
                        self.setState({
                            name: '',
                            password:""
                        });
                    }
                })
                .catch(function (error) { });
        }
    }
    render() {
        return (
            <div className="register-box">
                <div className="register-logo">
                    <a href="">
                        <b>Login</b>
                    </a>
                </div>

                <div className="card">
                    <div className="card-body register-card-body">
                        <form onSubmit={this.onSubmit}>
                            {this.state.error && (
                                <div className="alert alert-danger">
                                    <strong>{this.state.error}</strong>
                                </div>
                            )}
                            <div className="form-group has-feedback">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="User name"
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group has-feedback">
                                <input
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.onChangePassword}
                                    className="form-control"
                                    placeholder="Password"
                                />
                            </div>
                            <div className="row">
                                <div className="col-8">
                                    <div className="checkbox icheck" />
                                </div>
                                <div className="col-4">
                                    <button
                                        type="submit"
                                        className="btn btn-primary btn-block btn-flat"
                                    >
                                        Login
									</button>
                                </div>
                            </div>
                        </form>

                        <a href="/register" className="text-center">
                            I want to be a admin
						</a>
                    </div>
                </div>
            </div>
        );
    }
}
export default Login;
